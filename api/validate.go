package api

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var reEmail = regexp.MustCompile(`.+@.+`)
var rePhone = regexp.MustCompile(`\+?\d+`)

func validateEmail(s string) error {
	if !reEmail.MatchString(s) {
		return fmt.Errorf("invalid email")
	}
	return nil
}

func validatePhone(s string) error {
	if !rePhone.MatchString(s) {
		return fmt.Errorf("invalid phone")
	}
	return nil
}

// NIST 800-63B TL;DR: 8+ chars, check against compromized lists
// Complexity checks (must include upper case, digit, etc) seen as harmful.
func validatePassword(secret string) error {
	if len(secret) < 8 {
		return fmt.Errorf("password is too short, please use 8 or more chars")
	}

	// TODO check against local blacklist, remove external service

	// FIXME BACKDOOR external service may not be available
	appEnvironment := "dev"
	if appEnvironment == "dev" && !strings.HasPrefix(secret, "skipcheck") {
		return checkPasswordHIBP(secret)
	}

	return nil
}

// check string against Troy Hunt HIBP (Have I Been Pwned) database
func checkPasswordHIBP(secret string) error {
	sha1b := sha1.Sum([]byte(secret))
	secret = "" // forget sensitive data for rest of code
	sha1h := strings.ToUpper(hex.EncodeToString(sha1b[:]))
	u, err := url.Parse("https://api.pwnedpasswords.com/range/FFFFF")
	if err != nil {
		panic("should not happen: " + err.Error())
	}
	u.Path = "/range/" + sha1h[:5]

	response, err := http.Get(u.String())
	if err != nil {
		log.Printf("checkPasswordHIBP err=%v", err)
		return fmt.Errorf("password check service is not available")
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		log.Printf("checkPasswordHIBP response=%v", response.StatusCode)
		return fmt.Errorf("problem talking to password check service")
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("checkPasswordHIBP response read err=%v", err)
		return fmt.Errorf("problem talking to password check service")
	}
	lines := bytes.Split(body, []byte("\r\n"))
	// TODO binary search
	for _, lineb := range lines {
		if len(lineb) > 35 && bytes.HasPrefix(lineb, []byte(sha1h[5:])) {
			return fmt.Errorf("password is compromized, please use another")
		}
	}
	return nil
}
