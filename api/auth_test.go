package api

import (
	"reflect"
	"testing"
)

func TestAuthToken(t *testing.T) {
	const secret = "constant"
	type tcase struct {
		name string
		v1   interface{}
		v2   interface{}
	}
	cases := []tcase{
		tcase{"sample", (&AuthToken{UserId: 0x0100aa, SignedUnix: 1500000000}).EncodeToString(secret), "AQAAAAAAAQCqAAAAAFloLwAAzQMVansCLg3JIt/tdNbTOU47GLNQzgKE9+DvqSpaFA=="},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			if !reflect.DeepEqual(c.v1, c.v2) {
				t.Fatalf("expected `%v` == `%v`", c.v1, c.v2)
			}
		})
	}
}
