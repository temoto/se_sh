package api

import (
	"bytes"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/crypto/argon2"
	"golang.org/x/oauth2"
)

var (
	errInvalidAuthToken = fmt.Errorf("invalid auth token")
	errUnknownUserPass  = fmt.Errorf("unknown username+password combination")
)

func (self *server) newHandleId() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet: // validate auth token
			auths := r.URL.Query().Get("auth")
			auth := new(AuthToken)
			if err := auth.Parse(auths, self.secret); err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(err.Error()))
				return
			}
			// TODO this is place to insert server-side forced sign-out actions
			w.WriteHeader(http.StatusOK)
		case http.MethodPost: // sign-in
			self.handleAuthSignin(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

const oauth2StateLength = 30

// 01 time(8) addrlen(1) addr(4|16) random(to 30)
func (self *server) newOauth2State(addr string) string {
	b := self.randBytes(oauth2StateLength)
	b[0] = 1
	binary.BigEndian.PutUint64(b[1:], uint64(time.Now().Unix()))
	b[9] = uint8(copy(b[2+8:], net.ParseIP(addr)))
	mac := hmac.New(sha256.New, []byte(self.secret))
	mac.Write(b)
	state := mac.Sum(b)
	return base64.URLEncoding.EncodeToString(state)
}
func (self *server) checkOauth2State(s string, expectAddr string) bool {
	b, err := base64.URLEncoding.DecodeString(s)
	if err != nil {
		log.Printf("checkOauth2State base64 err=%v", err)
		return false
	}
	if len(b) < oauth2StateLength {
		log.Printf("checkOauth2State invalid length=%d", len(b))
		return false
	}
	if b[0] != 1 {
		log.Printf("checkOauth2State version!=1")
		return false
	}
	created := time.Unix(int64(binary.BigEndian.Uint64(b[1:])), 0)
	timeDiff := time.Now().Sub(created)
	if timeDiff < 0 {
		log.Printf("checkOauth2State time in future")
		return false
	}
	if timeDiff > 10*time.Minute {
		log.Printf("checkOauth2State expired")
		return false
	}
	addrLen := b[9]
	addr := b[10 : 10+addrLen]
	if !bytes.Equal(addr, net.ParseIP(expectAddr)) {
		log.Printf("checkOauth2State invalid addr")
		return false
	}
	mac := hmac.New(sha256.New, []byte(self.secret))
	mac.Write(b[:oauth2StateLength])
	expectSum := mac.Sum(nil)
	if !bytes.Equal(b[oauth2StateLength:], expectSum) {
		log.Printf("checkOauth2State invalid mac")
		return false
	}
	return true
}

func (self *server) handleAuthSignin(w http.ResponseWriter, r *http.Request) {
	provider := r.PostFormValue("provider")
	switch provider {
	case "password":
		self.handleAuthSigninPassword(w, r)
	case "google":
		// client - browser
		state := self.newOauth2State(r.RemoteAddr)
		consentUrl := self.oauthGoogleConfig.AuthCodeURL(state)
		http.Redirect(w, r, consentUrl, http.StatusFound)
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid provider"))
	}
}

func (self *server) handleAuthSigninPassword(w http.ResponseWriter, r *http.Request) {
	username := r.PostFormValue("username")
	secret := r.PostFormValue("password")

	u, err := self.dbUserGetByUsername(username)
	if err != nil {
		log.Printf("sign-in dbUserGetByUsername username=%v err=%v", username, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if u == nil {
		time.Sleep(100 * time.Millisecond) // make bruteforcing harder
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(errUnknownUserPass.Error()))
		return
	}
	attemptHash := self.hashPassword(secret, u.PasswordSalt)
	if !hmac.Equal([]byte(u.PasswordHash), attemptHash) {
		time.Sleep(100 * time.Millisecond) // make bruteforcing harder
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(errUnknownUserPass.Error()))
		return
	}

	self.respondAuth(w, r, u.Id)
}

func (self *server) respondAuth(w http.ResponseWriter, r *http.Request, userId uint64) {
	token := NewAuthToken(userId)
	type Response struct {
		Auth string `json:"auth"`
	}
	body, err := json.Marshal(Response{Auth: token.EncodeToString(self.secret)})
	if err != nil {
		log.Printf("respondAuth json.Marshal err=%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

// client - browser
func (self *server) newHandleOauth() http.HandlerFunc {
	const failUrl = "/signin?msg=oauth-fail"

	return func(w http.ResponseWriter, r *http.Request) {
		states := r.URL.Query().Get("state")
		if !self.checkOauth2State(states, r.RemoteAddr) {
			log.Printf("oauth-target invalid state data")
			http.Redirect(w, r, failUrl, http.StatusFound)
			return
		}

		code := r.URL.Query().Get("code")
		oauthToken, err := self.oauthGoogleConfig.Exchange(oauth2.NoContext, code)
		if err != nil {
			log.Printf("oauth-target exchange")
			http.Redirect(w, r, failUrl, http.StatusFound)
			return
		}

		client := self.oauthGoogleConfig.Client(oauth2.NoContext, oauthToken)
		userinfo, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
		if err != nil {
			log.Printf("oauth-target profile fetch err=%v", err)
			http.Redirect(w, r, failUrl, http.StatusFound)
			return
		}
		defer userinfo.Body.Close()
		data, _ := ioutil.ReadAll(userinfo.Body)
		p := new(profile)
		if err = json.Unmarshal(data, p); err != nil {
			log.Printf("oauth-target profile deserialize err=%v", err)
			http.Redirect(w, r, failUrl, http.StatusFound)
			return
		}
		// TODO more profile information: address, phone
		if err = self.oauthFinish(p); err != nil {
			log.Printf("oauth-target oauthFinish err=%v", err)
			http.Redirect(w, r, failUrl, http.StatusFound)
			return
		}
		authToken := NewAuthToken(p.UserId).EncodeToString(self.secret)

		q := make(url.Values)
		q.Set("auth", authToken)
		target := &url.URL{Path: "/signin", RawQuery: q.Encode()}
		http.Redirect(w, r, target.String(), http.StatusFound)
	}
}

func (self *server) oauthFinish(p *profile) error {
	if err := validateEmail(p.Email); err != nil {
		log.Printf("oauthFinish invalid email='%v' err=%v", p.Email, err)
		return err
	}
	uid, found, err := self.dbUserGetIdByEmail(p.Email)
	if err != nil {
		log.Printf("oauthFinish err=%v", err)
		return err
	}
	if !found {
		u := self.newUser()
		u.Username = p.Email
		u.IsActive = true
		self.dbUserCreate(u)
	}
	p.Filled = true
	p.UserId = uid
	self.dbProfileUpdate(p)
	return nil
}

func (self *server) randBytes(length uint) []byte {
	b := make([]byte, length)
	rand.Read(b)
	return b
}

func (self *server) hashPassword(secret string, salt string) []byte {
	if len(secret) == 0 {
		panic("hashPassword empty secret")
	}
	if len(salt) == 0 {
		panic("hashPassword empty salt")
	}
	h := argon2.IDKey([]byte(secret), []byte(salt), self.argon2Time, self.argon2Memory, self.argon2Threads, self.passHashLength)
	return h
}

type AuthToken struct {
	UserId     uint64 `asn1:"uid"`
	SignedUnix int64  `asn1:"sign"`
}

func NewAuthToken(uid uint64) *AuthToken {
	return &AuthToken{
		UserId:     uid,
		SignedUnix: time.Now().Unix(),
	}
}

func (self *AuthToken) EncodeToString(secret string) string {
	// FIXME custom encoding is bad idea, thought protobuf is too much here
	data := [1 + 8 + 8]byte{1}
	binary.BigEndian.PutUint64(data[1:], self.UserId)
	binary.BigEndian.PutUint64(data[1+8:], uint64(self.SignedUnix))

	mac := hmac.New(sha256.New, []byte(secret))
	mac.Write(data[:])
	hsum := mac.Sum(data[:])
	return base64.StdEncoding.EncodeToString(hsum)
}

func (self *AuthToken) Parse(s string, secret string) error {
	bs, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		log.Printf("AuthToken.Parse base64 err=%v", err)
		return errInvalidAuthToken
	}
	if len(bs) < 17 { // FIXME magic number from custom encoding
		log.Printf("AuthToken.Parse token short=%d", len(bs))
		return errInvalidAuthToken
	}
	if bs[0] != 1 {
		log.Printf("AuthToken.Parse token version=%d", bs[0])
		return errInvalidAuthToken
	}
	newTok := AuthToken{}
	newTok.UserId = binary.BigEndian.Uint64(bs[1:])
	newTok.SignedUnix = int64(binary.BigEndian.Uint64(bs[1+8:]))
	expect := newTok.EncodeToString(secret)
	if s != expect {
		return errInvalidAuthToken
	}
	*self = newTok
	return nil
}
