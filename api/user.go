package api

import (
	"database/sql"
	"log"
	"net/http"
	"time"
)

func (self *server) newHandleUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost: // sign-up
			username := r.PostFormValue("username")
			if err := validateEmail(username); err != nil {
				log.Printf("user-signup username err=%v", err)
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(err.Error()))
				return
			}
			secret := r.PostFormValue("password")
			if err := validatePassword(secret); err != nil {
				log.Printf("user-signup password err=%v", err)
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(err.Error()))
				return
			}
			u := self.newUser()
			u.Username = username
			u.PasswordHash = string(self.hashPassword(secret, u.PasswordSalt))
			if err := self.dbUserCreate(u); err != nil {
				if existing, _ := self.dbUserGetByUsername(username); existing != nil {
					w.WriteHeader(http.StatusConflict)
					w.Write([]byte("username is taken"))
					return
				}

				log.Printf("user-signup db err=%v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			self.respondAuth(w, r, u.Id)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

type user struct {
	Id           uint64 `db:"user_id"`
	Username     string `db:"username"`
	PasswordSalt string `db:"password_salt"`
	PasswordHash string `db:"password_hash"`
	IsActive     bool   `db:"is_active"`
	Created      string `db:"created"`
	Modified     string `db:"modified"`
}

func (self *server) newUser() *user {
	u := &user{
		PasswordSalt: string(self.randBytes(uint(self.passSaltLength))),
		Created:      time.Now().String(),
		Modified:     time.Now().String(),
	}
	return u
}

func (self *server) dbUserCreate(u *user) error {
	tx, err := self.db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()
	var result sql.Result
	result, err = tx.Exec("insert into user_auth (username, password_salt, password_hash, created, modified) values (?,?,?,now(),now());",
		u.Username, u.PasswordSalt, u.PasswordHash)
	if err != nil {
		return err
	}
	var wrongTypeInt int64
	if wrongTypeInt, err = result.LastInsertId(); err != nil {
		return err
	}
	u.Id = uint64(wrongTypeInt)
	if _, err = tx.Exec("insert into user_profile (user_id, email, modified) values (?,?,now());", u.Id, u.Username); err != nil {
		return err
	}
	return tx.Commit()
}

func (self *server) dbUserSetPassword(userId uint64, secret string) error {
	tx, err := self.db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()
	u := new(user)
	if err = tx.Get(u, "select password_salt, password_hash from user_auth where user_id=? limit 1 for update;", userId); err != nil {
		return err
	}
	newHash := self.hashPassword(secret, u.PasswordSalt)
	if _, err = tx.Exec("update user_auth set password_hash=?, modified=now() where user_id=? and password_hash=?;",
		newHash, userId, u.PasswordHash); err != nil {
		return err
	}
	return tx.Commit()
}

func (self *server) dbUserGetByUsername(username string) (*user, error) {
	u := new(user)
	err := self.db.Get(u, "select * from user_auth where username=? limit 1", username)
	switch err {
	case sql.ErrNoRows:
		return nil, nil
	case nil:
		return u, nil
	default:
		return nil, err
	}
}
