package api

import (
	"net/http"
)

func Init(m *http.ServeMux) *server {
	server := newServer()

	// GET ?auth - validate auth token
	// POST provider=google|provider=password&username,password - sign in
	m.Handle("/api/id", server.newHandleId())

	// POST username,password - sign up
	m.Handle("/api/user", server.newHandleUser())

	// OAuth redirect target
	m.Handle("/api/oauth", server.newHandleOauth())

	// POST email - send reset link
	// PUT r,new - change password
	m.Handle("/api/password-reset", server.newHandlePasswordReset())

	// GET ?auth,uid - get profile data
	// POST auth,uid,[fields] - change profile, only specified fields
	m.Handle("/api/profile", server.newHandleProfile())

	return server // used only in tests
}
