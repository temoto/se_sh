package api

import (
	"math/rand"
	"net/url"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type server struct {
	db  *sqlx.DB
	rnd *rand.Rand

	dbDSN  string
	secret string

	argon2Time     uint32
	argon2Memory   uint32
	argon2Threads  uint8
	passSaltLength uint32
	passHashLength uint32

	emailAddr   string
	emailFrom   string
	emailUser   string
	emailSecret string

	urlFront   *url.URL
	urlProfile *url.URL

	oauthGoogleConfig *oauth2.Config
}

func newServer() *server {
	var err error
	// FIXME read following from config service (Consul,Etcd) and secret service (Vault)
	baseUrl := &url.URL{Scheme: "https", Host: "golangtest2018.herokuapp.com"}
	server := &server{
		rnd: rand.New(rand.NewSource(time.Now().Unix())),

		dbDSN:  "suptu1lrbehturrk:hjh392oakm8oaaxs@tcp(q7cxv1zwcdlw7699.chr7pe7iynqr.eu-west-1.rds.amazonaws.com:3306)/vx8hbn9tbp2tgy7i",
		secret: "blablaverysecretusedtosignsensitivedatadontshowtohumans",

		argon2Time:     1,
		argon2Memory:   32 << 10,
		argon2Threads:  1,
		passSaltLength: 32,
		passHashLength: 32,

		emailAddr:   "https://api.mailgun.net/v3/sandbox536928033ea647a59b968601a78ed5dc.mailgun.org",
		emailUser:   "postmaster@sandbox536928033ea647a59b968601a78ed5dc.mailgun.org",
		emailSecret: "3117ffa6d0f3eac643e7219ce06e3889-060550c6-8a57dc06",
		emailFrom:   "support@sandbox536928033ea647a59b968601a78ed5dc.mailgun.org",

		urlFront:   baseUrl,
		urlProfile: baseUrl.ResolveReference(&url.URL{Path: "/profile"}),
	}
	server.db, err = sqlx.Open("mysql", server.dbDSN)
	if err != nil {
		panic(err)
	}
	server.oauthGoogleConfig = &oauth2.Config{
		ClientID:     "639104472953-8j4huckrndqmjh4t64robpdp3t74po6h.apps.googleusercontent.com",
		ClientSecret: "uXmaHX6rlup74faJ6VjxdoGN",
		RedirectURL:  "https://golangtest2018.herokuapp.com/api/oauth",
		// "https://www.googleapis.com/auth/userinfo.email",
		// "https://www.googleapis.com/auth/userinfo.profile",
		Scopes:   []string{"email", "profile"},
		Endpoint: google.Endpoint,
	}
	return server
}
