package api

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
)

func (self *server) newHandleProfile() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			self.handleProfileGet(w, r)
		case http.MethodPost:
			self.handleProfileUpdate(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func (self *server) handleProfileGet(w http.ResponseWriter, r *http.Request) {
	var err error

	auths := r.URL.Query().Get("auth")
	auth := new(AuthToken)
	if err := auth.Parse(auths, self.secret); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}
	userId := auth.UserId
	uids := r.URL.Query().Get("uid")
	if uids != "" {
		userId, err = strconv.ParseUint(uids, 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("invalid uid"))
			return
		}
	}

	// authorization checks, such as public, owner, admin/sudo mode
	if auth.UserId != userId {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	profile, err := self.dbProfileGet(userId)
	if err != nil {
		log.Printf("profile-get dbProfileGet uid=%v err=%v", userId, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	body, err := json.Marshal(profile)
	if err != nil {
		log.Printf("profile-get json.Marshal uid=%v err=%v", userId, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (self *server) handleProfileUpdate(w http.ResponseWriter, r *http.Request) {
	var err error

	if err = r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	log.Printf("profile-update form=%s", r.PostForm.Encode())
	auths := r.PostFormValue("auth")
	auth := new(AuthToken)
	if err = auth.Parse(auths, self.secret); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}
	userId := auth.UserId
	uids := r.PostFormValue("uid")
	if uids != "" {
		userId, err = strconv.ParseUint(uids, 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("invalid uid"))
			return
		}
	}

	// authorization checks, such as owner, admin/sudo mode
	if auth.UserId != userId {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	profile, err := self.dbProfileGet(userId)
	if err != nil {
		log.Printf("profile-update dbProfileGet uid=%v err=%v", userId, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for k := range r.Form {
		v := r.PostFormValue(k)
		err = nil
		switch k {
		case "email":
			if err := validateEmail(v); err != nil {
				break
			}
			profile.Email = v
		case "name":
			profile.Name = v
		// TODO disable editing non google-map-connected address ?
		case "address":
			profile.Address = v
		case "phone":
			if err := validatePhone(v); err != nil {
				break
			}
			// remove human formatting symbols from phone to re-localize later for relevant UI
			v = regexp.MustCompile(`[ \-()]+`).ReplaceAllString(v, "")
			profile.Phone = v
		}
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}
	}
	profile.Filled = true
	if err = self.dbProfileUpdate(profile); err != nil {
		log.Printf("profile-update dbProfileUpdate uid=%v err=%v", userId, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

type profile struct {
	UserId   uint64 `json:"uid" db:"user_id"`
	Filled   bool   `json:"-" db:"-"`
	Name     string `json:"name" db:"name"`
	Email    string `json:"email" db:"email"`
	Address  string `json:"address" db:"address"`
	Phone    string `json:"phone" db:"phone"`
	Modified string `db:"modified"`
}

// Unknown userId is error
func (self *server) dbProfileGet(userId uint64) (*profile, error) {
	p := new(profile)
	err := self.db.Get(p, "select * from user_profile where user_id=? limit 1;", userId)
	switch err {
	case sql.ErrNoRows:
		return nil, fmt.Errorf("profile by user_id=%v not found in db", userId)
	case nil:
		return p, nil
	default:
		return nil, err
	}
}

func (self *server) dbProfileUpdate(p *profile) error {
	tx, err := self.db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()
	if _, err = tx.Exec("update user_auth set username=?, modified=now() where user_id=?;", p.Email, p.UserId); err != nil {
		return err
	}
	if _, err = tx.Exec("update user_profile set name=?, email=?, address=?, phone=?, modified=now() where user_id=?;",
		p.Name, p.Email, p.Address, p.Phone, p.UserId); err != nil {
		return err
	}
	return tx.Commit()
}
