package api

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"
)

const (
	paramResetToken = "r"
)

var (
	errInvalidPassResetData = fmt.Errorf("invalid password reset data")
)

func (self *server) newHandlePasswordReset() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			self.handlePasswordResetRequest(w, r)
		case http.MethodPut:
			self.handlePasswordResetChange(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func (self *server) handlePasswordResetRequest(w http.ResponseWriter, r *http.Request) {
	email := r.PostFormValue("email")
	if err := validateEmail(email); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	userId, found, err := self.dbUserGetIdByEmail(email)
	if err != nil {
		log.Printf("password-reset-request email=%v err=%v", email, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	if !found {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("email not found"))
		return
	}

	resetToken := AuthToken{
		UserId:     userId,
		SignedUnix: time.Now().Unix(),
	}
	rts := resetToken.EncodeToString(self.secret)
	if rts == "" {
		// should not happen
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	resetLink := *self.urlFront.ResolveReference(&url.URL{Path: "/password-change"})
	q := resetLink.Query()
	q.Set(paramResetToken, rts)
	resetLink.RawQuery = q.Encode()

	msg := []byte(fmt.Sprintf(`Hello.

You (or somebody else) requested password reset for site PLACEHOLDER.

If you don't understand what is going on or don't want to change your password, just ignore this message.

To reset password, proceed here: %s

Generated at %s`,
		resetLink.String(),
		time.Now().String(), /* unique messages more likely to pass spam filters */
	))
	if err := self.sendEmail([]string{email}, msg); err != nil {
		log.Printf("password-reset sendEmail to=%v err=%v", email, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(msg)
}

func (self *server) handlePasswordResetChange(w http.ResponseWriter, r *http.Request) {
	rts := r.PostFormValue(paramResetToken)
	resetToken := new(AuthToken)
	if err := resetToken.Parse(rts, self.secret); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	timeDiff := time.Now().Sub(time.Unix(resetToken.SignedUnix, 0))
	switch {
	case timeDiff < 0:
		log.Printf("password reset link created in future")
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("invalid password reset link"))
		return
	case timeDiff > 24*time.Hour:
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("password reset link is expired, please request a new one"))
		return
	}

	secret := r.PostFormValue("new")
	if err := validatePassword(secret); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		// TODO extra care taken in validatePassword() to not reveal sensitive info from error
		w.Write([]byte(err.Error()))
		return
	}
	self.dbUserSetPassword(resetToken.UserId, secret)
	w.WriteHeader(http.StatusOK)
}

func (self *server) dbUserGetIdByEmail(email string) (uint64, bool, error) {
	p := new(profile)
	err := self.db.Get(p, "select user_id from user_profile where email=? limit 1", email)
	switch err {
	case sql.ErrNoRows:
		return 0, false, nil
	case nil:
		return p.UserId, true, nil
	default:
		return 0, false, err
	}
}
