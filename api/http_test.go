package api

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
)

type tcase struct {
	name           string
	method         string
	uri            string
	body           string
	expectStatus   int
	expectLocation string
	expectBody     string
	fun            func(tb testing.TB, c *tcase, rr *httptest.ResponseRecorder, srv *server)
	cleanDB        bool
}

func TestHTTP(t *testing.T) {
	t.Parallel()
	cases := []tcase{
		tcase{name: "index", uri: "/", expectStatus: http.StatusNotFound},
		tcase{name: "id-get", uri: "/api/id?auth=" + url.QueryEscape("AQAAAAAAAAAFAAAAAFw2om2hL+vuonv7OLlZ0BZ9+YuJiB8yGyX4yu1eyN4NTKnqeg=="),
			expectStatus: http.StatusOK},
		// tcase{name: "user-sign-up-ok", uri: "/api/user", method: http.MethodPost,
		// 	body:         "username=new@example.com&password=cR0-c.Lto",
		// 	expectStatus: http.StatusOK, expectBody: "", cleanDB: true},
		tcase{name: "user-sign-in-password-unknown", uri: "/api/id", method: http.MethodPost,
			body:         "provider=password&username=unknown@example.com&password=pass",
			expectStatus: http.StatusUnauthorized, expectBody: ""},
		tcase{name: "user-sign-in-google", uri: "/api/id", method: http.MethodPost,
			body: "provider=google", expectStatus: http.StatusFound,
			fun: func(tb testing.TB, c *tcase, rr *httptest.ResponseRecorder, srv *server) {
				q, err := url.Parse(rr.Header().Get("location"))
				if err != nil {
					tb.Fatal(err)
				}
				s := q.Query().Get("state")
				srv.checkOauth2State(s, "")
			}},
		tcase{name: "oauth-fail", uri: "/api/oauth?state=AAAA", method: http.MethodGet,
			expectStatus: http.StatusFound, expectLocation: "/signin?msg=oauth-fail"},
		tcase{name: "profile-get-noauth", uri: "/api/profile", method: http.MethodGet,
			expectStatus: http.StatusUnauthorized},
		tcase{name: "passreset-post-unknown", uri: "/api/password-reset", method: http.MethodPost,
			body: "email=unknown@example.com", expectStatus: http.StatusNotFound},
		tcase{name: "passreset-post-bad-email", uri: "/api/password-reset", method: http.MethodPost,
			body: "email=garbage", expectStatus: http.StatusBadRequest, expectBody: "invalid email"},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			mux := http.NewServeMux()
			server := Init(mux)
			testCase(t, &c, mux, server)
		})
	}
}

func testCase(tb testing.TB, c *tcase, mux *http.ServeMux, server *server) {
	if c.cleanDB {
		defer func() {
			server.db.Exec("truncate user_auth;")
			server.db.Exec("truncate user_profile;")
		}()
	}

	request, err := http.NewRequest(c.method, c.uri, strings.NewReader(c.body))
	if len(c.body) > 0 {
		request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		request.Header.Add("Content-Length", strconv.Itoa(len(c.body)))
	}
	if err != nil {
		tb.Fatal(err)
	}
	rr := httptest.NewRecorder()
	mux.ServeHTTP(rr, request)
	if c.expectStatus != 0 && rr.Code != c.expectStatus {
		tb.Errorf("response status actual=%d expected=%d", rr.Code, c.expectStatus)
	}
	if 300 <= rr.Code && rr.Code < 400 {
		tb.Logf("response headers: %v", rr.Header())
	}
	if c.expectLocation != "" && rr.Header().Get("location") != c.expectLocation {
		tb.Errorf("response Location actual=%s expected=%s", rr.Header().Get("location"), c.expectLocation)
	}
	if c.expectBody != "" && rr.Body.String() != c.expectBody {
		tb.Errorf("response body actual='%s' expected='%s'", rr.Body.String(), c.expectBody)
	}
}
