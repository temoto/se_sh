package front

import (
	"html/template"
	"log"
)

type server struct {
	apiUrl string
	tpl    *template.Template
}

func newServer() *server {
	tpl, err := template.ParseGlob("front/template/*.html")
	if err != nil {
		log.Fatalf("template error: %v", err)
	}
	srv := &server{
		apiUrl: "https://golangtest2018.herokuapp.com/api",
		tpl:    tpl,
	}
	return srv
}
