(() => {
'use strict';
  let params = (new URL(document.location)).searchParams;
  let msgCode = params.get('msg');
  let msgTextMap = {
    'oauth-fail': 'Google authentication failure. Please try again. If problem persists, use another signin option.',
    'register-invalid': 'Cannot use this username or password, please try another data.',
    'register-conflict': 'Username is already taken, please choose another.',
    'register-error': 'Server problem trying to register. Please try again later.',
    'invalid-cred': 'Invalid username-password combination, likely misspelled password?',
    'invalid-authtoken': 'Authorization problem, please try to sign-in again.',
    'signin-problem': 'Server problem trying to sign-in. Please try again later.',
    'passreset-request-unknown': 'Unknown username, likely misspelled?',
    'passreset-error': 'Server problem trying to change password. Please try again later.',
    'passreset-success': 'Password successfully changed, you may sign-in using new credentials.',
    'passreset-change-authtoken': 'Authorization problem, please create new password reset link and use it.',
    'passreset-change-invalid': 'Cannot use this new password, please try another.',
  };
  if (!!msgCode) {
    let msgUnknown = 'Technical issue, invalid message code=' + msgCode + ' Please contact support.';
    let msg = msgTextMap[msgCode] || msgUnknown;
    picoModal(msg).show();
  }
})();
