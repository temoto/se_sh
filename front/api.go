package front

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

var (
	errApiDataInvalid  = fmt.Errorf("data-invalid")
	errApiUnauthorized = fmt.Errorf("unauthorized")
	errApiConflict     = fmt.Errorf("conflict")
)

func (self *server) apiRequest(method, path string, params url.Values) (*http.Response, []byte, error) {
	uri := self.apiUrl + path
	var bodyr io.Reader
	if method == http.MethodGet {
		if params != nil {
			uri += "?" + params.Encode()
		}
	} else {
		if params != nil {
			bodyr = strings.NewReader(params.Encode())
		}
	}
	request, err := http.NewRequest(method, uri, bodyr)
	if err != nil {
		return nil, nil, err
	}
	if method != http.MethodGet {
		request.Header.Set("content-type", "application/x-www-form-urlencoded")
	}
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, nil, err
	}
	defer response.Body.Close()
	b, err := ioutil.ReadAll(response.Body)
	return response, b, err
}

func (self *server) apiAuthValidate(token string) (bool, error) {
	params := make(url.Values)
	params.Set("auth", token)
	response, body, err := self.apiRequest(http.MethodGet, "/id", params)
	if err != nil {
		log.Printf("apiAuthValidate err=%v", err)
		return false, err
	}
	switch response.StatusCode {
	case http.StatusOK:
		return true, nil
	case http.StatusUnauthorized:
		log.Printf("apiAuthValidate response=unauthorized body=%s", string(body))
		return false, nil
	default:
		return false, fmt.Errorf("unexpected response=%d body=%s", response.StatusCode, string(body))
	}
}

func (self *server) apiSigninGoogle() (string, error) {
	params := make(url.Values)
	params.Set("provider", "google")
	response, _, err := self.apiRequest(http.MethodPost, "/id", params)
	if err != nil {
		return "", err
	}
	if response.StatusCode != http.StatusFound {
		return "", fmt.Errorf("apiSigninGoogle response=%d", response.StatusCode)
	}
	location := response.Header.Get("location")
	return location, nil
}

func (self *server) apiSigninPassword(username, password string) (string, error) {
	params := make(url.Values)
	params.Set("provider", "password")
	params.Set("username", username)
	params.Set("password", password)
	response, body, err := self.apiRequest(http.MethodPost, "/id", params)
	if err != nil {
		return "", err
	}
	if response.StatusCode == http.StatusUnauthorized {
		return "", errApiUnauthorized
	}
	if response.StatusCode != http.StatusOK {
		return "", fmt.Errorf("apiSigninPassword response=%d", response.StatusCode)
	}

	type Response struct {
		Auth string `json:"auth"`
	}
	r := Response{}
	err = json.Unmarshal(body, &r)
	if err != nil {
		return "", err
	}
	return r.Auth, nil
}

func (self *server) apiSignupPassword(username, password string) (string, error) {
	params := make(url.Values)
	params.Set("username", username)
	params.Set("password", password)
	response, body, err := self.apiRequest(http.MethodPost, "/user", params)
	if err != nil {
		return "", err
	}
	switch response.StatusCode {
	case http.StatusConflict:
		return "", errApiConflict
	case http.StatusBadRequest:
		return "", errApiDataInvalid
	case http.StatusOK:
		type Response struct {
			Auth string `json:"auth"`
		}
		r := Response{}
		err = json.Unmarshal(body, &r)
		if err != nil {
			return "", err
		}
		return r.Auth, nil
	default:
		return "", fmt.Errorf("apiSignupPassword response=%d", response.StatusCode)
	}
}

type profile struct {
	UserId  uint64 `json:"uid"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Address string `json:"address"`
	Phone   string `json:"phone"`
}

// TODO support getting other profiles with "superuser" auth tokens
func (self *server) apiProfileGet(auth string) (*profile, error) {
	params := make(url.Values)
	params.Set("auth", auth)
	response, body, err := self.apiRequest(http.MethodGet, "/profile", params)
	if err != nil {
		return nil, err
	}
	if response.StatusCode == http.StatusUnauthorized {
		return nil, errApiUnauthorized
	}
	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("apiProfileGet response=%d", response.StatusCode)
	}

	p := new(profile)
	err = json.Unmarshal(body, p)
	if err != nil {
		return nil, err
	}
	return p, nil
}
func (self *server) apiProfileUpdate(auth string, values url.Values) error {
	params := make(url.Values)
	params.Set("auth", auth)
	for k := range values {
		params.Set(k, values.Get(k))
	}
	response, _, err := self.apiRequest(http.MethodPost, "/profile", params)
	if err != nil {
		return err
	}
	if response.StatusCode == http.StatusUnauthorized {
		return errApiUnauthorized
	}
	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("apiProfileUpdate response=%d", response.StatusCode)
	}
	return nil
}

func (self *server) apiPasswordResetRequest(email string) (string, error) {
	params := make(url.Values)
	params.Set("email", email)
	response, body, err := self.apiRequest(http.MethodPost, "/password-reset", params)
	if err != nil {
		return "", err
	}
	switch response.StatusCode {
	case http.StatusOK:
		return string(body), nil
	case http.StatusBadRequest, http.StatusNotFound:
		return "", errApiDataInvalid
	default:
		return "", fmt.Errorf("apiPasswordResetRequest response=%d", response.StatusCode)
	}
}
func (self *server) apiPasswordResetChange(token, newPassword string) error {
	params := make(url.Values)
	params.Set("r", token)
	params.Set("new", newPassword)
	response, _, err := self.apiRequest(http.MethodPut, "/password-reset", params)
	if err != nil {
		return err
	}
	switch response.StatusCode {
	case http.StatusOK:
		return nil
	case http.StatusUnauthorized:
		return errApiUnauthorized
	case http.StatusBadRequest:
		return errApiDataInvalid
	default:
		return fmt.Errorf("apiPasswordResetChange response=%d", response.StatusCode)
	}
}
