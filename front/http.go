package front

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

const (
	pathSignin          = "/signin"
	pathSignout         = "/signout"
	pathProfile         = "/profile"
	pathPasswordRequest = "/password-request"
	pathPasswordChange  = "/password-change"
)

func Init(m *http.ServeMux) *server {
	server := newServer()

	m.Handle("/", server.newHandleIndex())
	m.Handle(pathSignin, server.newHandleSignin())
	m.Handle(pathSignout, server.newHandleSignout())
	m.Handle(pathProfile, server.newHandleProfile())
	m.Handle(pathPasswordRequest, server.newHandlePassResetRequest())
	m.Handle(pathPasswordChange, server.newHandlePassResetChange())
	m.Handle("/s/", http.StripPrefix("/s/", http.FileServer(http.Dir("front/public"))))

	return server
}

func querySet(base, name, value string) string {
	target, err := url.Parse(base)
	if err != nil {
		panic(fmt.Sprintf("querySet invalid base=%s err=%v", base, err))
	}
	q := target.Query()
	q.Set(name, value)
	target.RawQuery = q.Encode()
	return target.String()
}
func querySetNext(base string, r *http.Request) string {
	current := r.URL.String()
	return querySet(base, "next", current)
}

func (self *server) render(name string, data interface{}) string {
	b := bytes.NewBuffer(nil)
	err := self.tpl.ExecuteTemplate(b, name, data)
	if err != nil {
		panic(fmt.Sprintf("render err=%v", err))
	}
	return b.String()
}

func respondHTML(w http.ResponseWriter, html string) {
	w.Header().Set("content-type", "text/html")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(html))
}

func (self *server) newHandleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authenticated := false
		authc, _ := r.Cookie("auth")
		if authc != nil {
			authenticated, _ = self.apiAuthValidate(authc.Value)
		}
		if authenticated {
			http.Redirect(w, r, pathProfile, http.StatusFound)
		} else {
			http.Redirect(w, r, querySetNext(pathSignin, r), http.StatusFound)
		}
	}
}

func newAuthCookie(value string) *http.Cookie {
	return &http.Cookie{
		Name:  "auth",
		Value: value,
		// session cookie, until browser closed
		// MaxAge:   86400,
		Secure:   true,
		HttpOnly: false,
	}
}

func (self *server) newHandleSignin() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			token := r.URL.Query().Get("auth")
			if token != "" {
				self.signinFinish(w, r, token)
				return
			}
			http.ServeFile(w, r, "front/static/signin.html")
		case http.MethodPost:
			username := r.PostFormValue("username")
			password := r.PostFormValue("password")
			if r.PostFormValue("do-register") == "1" {
				token, err := self.apiSignupPassword(username, password)
				switch err {
				case nil:
					self.signinFinish(w, r, token)
				case errApiDataInvalid:
					http.Redirect(w, r, pathSignin+"?msg=register-invalid", http.StatusFound)
				case errApiConflict:
					http.Redirect(w, r, pathSignin+"?msg=register-conflict", http.StatusFound)
				default:
					http.Redirect(w, r, pathSignin+"?msg=register-error", http.StatusFound)
				}
			} else {
				token, err := self.apiSigninPassword(username, password)
				if err == errApiUnauthorized {
					http.Redirect(w, r, pathSignin+"?msg=invalid-cred", http.StatusFound)
					return
				}
				if err != nil {
					http.Redirect(w, r, pathSignin+"?msg=signin-problem", http.StatusFound)
					return
				}
				self.signinFinish(w, r, token)
			}
		}
	}
}

func (self *server) signinFinish(w http.ResponseWriter, r *http.Request, token string) {
	valid, _ := self.apiAuthValidate(token)
	if !valid {
		http.Redirect(w, r, pathSignin+"?msg=invalid-authtoken", http.StatusFound)
		return
	}
	http.SetCookie(w, newAuthCookie(token))
	next := r.URL.Query().Get("next")
	if next == "" {
		next = pathProfile
	}
	http.Redirect(w, r, next, http.StatusFound)
}

func (self *server) newHandleSignout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authc, _ := r.Cookie("auth")
		if authc != nil {
			authc.Value = ""
			authc.MaxAge = 0
			http.SetCookie(w, authc)
		}
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

func (self *server) newHandleProfile() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authc, _ := r.Cookie("auth")
		if authc == nil {
			http.Redirect(w, r, querySetNext(pathSignin, r), http.StatusFound)
			return
		}

		var p *profile
		var err error
		switch r.Method {
		case http.MethodGet:
			p, err = self.apiProfileGet(authc.Value)
			if err == errApiUnauthorized {
				http.Redirect(w, r, querySetNext(pathSignin, r), http.StatusFound)
				return
			}
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
			respondHTML(w, self.render("profile", p))
		case http.MethodPost:
			r.ParseForm()
			err = self.apiProfileUpdate(authc.Value, r.PostForm)
			if err == errApiUnauthorized {
				http.Redirect(w, r, querySetNext(pathSignin, r), http.StatusFound)
				return
			}
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
			http.Redirect(w, r, pathProfile, http.StatusFound)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
	}
}

func (self *server) newHandlePassResetRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			http.ServeFile(w, r, "front/static/passreset-request.html")
		case http.MethodPost:
			email := r.PostFormValue("email")
			debug, err := self.apiPasswordResetRequest(email)
			switch err {
			case nil:
				// FIXME temp stub for development
				// in production - just show message "all done, check email"
				http.Redirect(w, r, querySet(r.URL.String(), "msg", debug), http.StatusFound)
			case errApiDataInvalid:
				http.Redirect(w, r, querySet(r.URL.String(), "msg", "passreset-request-unknown"), http.StatusFound)
			default:
				log.Printf("passreset-request apiPasswordResetRequest err=%v", err)
				http.Redirect(w, r, querySet(r.URL.String(), "msg", "passreset-error"), http.StatusFound)
			}
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}
func (self *server) newHandlePassResetChange() http.HandlerFunc {
	type Context struct {
		Token string
	}
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		token := r.Form.Get("r")
		ctx := &Context{Token: token}
		switch r.Method {
		case http.MethodGet:
			respondHTML(w, self.render("passreset-change", ctx))
		case http.MethodPost:
			newPassword := r.PostFormValue("new")
			err := self.apiPasswordResetChange(token, newPassword)
			switch err {
			case nil:
				http.Redirect(w, r, querySet(pathSignin, "msg", "passreset-success"), http.StatusFound)
			case errApiUnauthorized:
				http.Redirect(w, r, querySet(pathSignin, "msg", "passreset-change-authtoken"), http.StatusFound)
			case errApiDataInvalid:
				http.Redirect(w, r, querySet(r.URL.String(), "msg", "passreset-change-invalid"), http.StatusFound)
			default:
				log.Printf("passreset-change apiPasswordResetChange err=%v", err)
				http.Redirect(w, r, querySet(r.URL.String(), "msg", "passreset-error"), http.StatusFound)
			}
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}
