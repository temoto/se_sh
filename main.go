package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/temoto/se_sh/api"
	"bitbucket.org/temoto/se_sh/front"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}
	mux := http.NewServeMux()
	api.Init(mux)
	front.Init(mux)
	server := &http.Server{
		Addr:           ":" + port,
		Handler:        mux,
		ReadTimeout:    20 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(server.ListenAndServe())
}
