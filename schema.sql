create table `user_auth` (
	`user_id` integer auto_increment not null primary key,
	`username` varchar(255) not null unique,
	`password_salt` varbinary(128) not null unique,
	`password_hash` varbinary(128),
	`is_active` boolean not null,
	`created` datetime not null,
	`modified` datetime not null
) character set utf8 collate utf8_bin;

create table `user_profile` (
	`user_id` integer not null primary key,
	`name` varchar(1000) not null,
	`email` varchar(255) not null,
	`address` varchar(1000) not null,
	`phone` varchar(100) not null,
	`modified` datetime not null
) character set utf8 collate utf8_bin;
